import React from 'react'
import './App.css';
import Task from './task'
import Tasklist from './tasklist'
import Addtask from './addtask'

class App extends React.Component {
  counter = 0 
  state = {
    tasks: [
      {
        id:0,
        text: 'zagrac wreszczie w Wiedzmian 3',
        date: '2018-02-2015',
        important:true,
        active: false,
        finishDate : null,
      },
      {
      id:1,
      text: 'Programowac',
      date: '2018-02-2018',
      important:true,
      active: true,
      finishDate : null,
      }
    ]
  }
  handleDeleteTask = (id,e) => {
    const tasks = [...this.state.tasks];
    const value = e.target.value;
    tasks.splice(value,1);
    console.log(tasks);
    this.setState({
      tasks
    })
  }
  
  changeTaskStatus =(id,e)=>{
    const tasks = Array.from(this.state.tasks)
    tasks.forEach(task =>{
      if(task.id === id ){
        task.active = false;
        task.finishDate = new Date().getTime();
      }
    })
    this.setState({
      tasks
    })
  }
  addTask = (text,date,important) => {
    const task = {
      id:this.counter,
      text: text, // z inputa text/date/important
      date: date,
      important: important,
      active: true,
      finishDate : null,
    }
    this.counter++

    this.setState(prevState=>({
      tasks:[...prevState.tasks,task]
    }))
    return true

  }


  render(){
    return (
    <div>
      Todoapp
      <Addtask add={this.addTask}/>
      <Tasklist tasks={this.state.tasks}delete={this.handleDeleteTask} change={this.changeTaskStatus}/>
    </div>
  );
}
}

export default App;
