import React from "react"

const Task = (props) => {
    const {id,text,date,important,active,finishDate} = props.tasks
    const style ={
        color : "red",
    }
    if(props.active){
    return(
<div>
    <p>
        <strong style={important ? style : null}>{text}</strong> - zrobic do {date}
        <br/>
        -potwierdzenie wykonania<span>{finishDate}</span>
        <button onClick={()=>props.change(id)}>Zostalo zrobione</button>
    <button onClick={()=>props.delete(id)}>X</button>
    </p>

</div>
)}else{
    const finish = new Date(finishDate).toLocaleString()
    return (
        <div>
            <p><strong>{text}</strong>Zrobic do {date}<br/>
            -potwierdzenie wykonania <span>{finish}</span>
            </p>
        </div>
    )
}

    }

export default Task
