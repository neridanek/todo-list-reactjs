import React from "react"
import Task from "./task"

const Tasklist = (props) => {

const active = props.tasks.filter(task =>task.active === true)
const done = props.tasks.filter(task =>!task.active)

const activeTasks = active.map(task =><Task key={task.id}
task={task} delete={props.delete} change={props.change}/>)
const doneTasks = done.map(task =><Task key={task.id}
    task={task} delete={props.delete} change={props.change}/>)

if (done.length >=2){
    done.sort((a,b)=>{
        if(a.finishDate < b.finishDate){
            return 1
        }
        if(a.finishDate > b.finishDate){
            return -1
        }
        return 0;
    })
}
if (active.length >=2){
    active.sort((a,b)=>{
        if(a.text < b.text){
            return 1
        }
        if(a.text > b.text){
            return -1
        }
        return 0
    })
}

return(
    <>
<div>
<h1>Zadania do zrobienia</h1>
{activeTasks.length > 0 ? activeTasks : <p>Brak zadan</p>}
</div>
<div>
    <h1>Zadania zrobione</h1>
    {done.length>5 && <span style={{fontSize:10}}>Wyswietlone 5 ostatnich elementow</span>}
    {doneTasks.slice(0,5)}
</div>
</>
)
}

export default Tasklist